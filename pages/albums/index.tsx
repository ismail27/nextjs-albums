import {AlbumsDao} from "../../dao/albums-dao";
import {Album} from "../../models/album";

interface IAlbumsProps {
    albums: Album[];
}

export default function Albums(props: IAlbumsProps) {
    return <main>
        <table>
            {props.albums.map(album => {
                return <td>
                    <tr>{album.title}</tr>
                    <tr>{album.year}</tr>
                </td>
            })}
        </table>
    </main>
}

export async function getStaticProps(context: any) {
    const albums: Album[] = await AlbumsDao.getAllAlbums();
    if (!albums) {
        return {
            redirect: {
                destination: '/',
                permanent: false,
            },
        }
    }
    return {
        props: {albums},
    }
}
